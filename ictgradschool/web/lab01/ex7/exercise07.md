Carnivores:

* Cat
* Dog
* Polar Bear
* Lion
* Fox

Omnivores:

* Fennec Fox
* Rat
* Hawk

Herbivores:

* Capybara
* Raccoon
* Chicken
* Horse
* Cow
* Sheep
* Frog

